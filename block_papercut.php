<?php
class block_papercut extends block_base {
    public function init() {
        $this->title = get_string('papercut', 'block_papercut');
    }
    function has_config() {
        return true;
    }
    // The PHP tag and the curly bracket for the class definition 
    // will only be closed after there is another function added in the next section.
    public function get_content() {
    global $USER;
    if ($this->content !== null) {
      return $this->content;
    }
    // Get variable from papercut server via URL example:
    // https://papercut.domain.local/rpc/api/web/user/administrator/details.js
    $serverName = get_config('papercut', 'ServerName');
    $c = new curl;
    $papercut = $c->get('https://' . $serverName . '/rpc/api/web/user/' . $USER->username . '/details.js');

    $text = '<script type="text/javascript">';
    $text .= $papercut;
    $text .= '</script>';
    $text .= '<div id="papercut-warn">';
    $text .= '<div id="papercut-progress" class="progress progress-striped">';
    $text .= '<div id="papercut-progress-success" class="bar bar-success" role="progressbar" style="width:100%"></div>';
    $text .= '<div id="papercut-progress-warning" class="bar bar-warning" role="progressbar" style="width:66%"></div>';
    $text .= '<div id="papercut-progress-danger" class="bar bar-danger" role="progressbar" style="width:33%"></div>';
    $text .= '</div>';
    $text .= '<div id="papercut-message1" class="" style="color:white;">message1</div>';
    $text .= '<div id="papercut-message2" class="" style="color:white;">message2</div>';
    $text .= '</div>';
    $text .= '<div id="papercut-ok">' . get_string('papercut-ok-default', 'block_papercut') . '</div>';
    $text .= '<script type="text/javascript">';
    $text .= 'if (pcUserDetails.balance >= 3) {';
    $text .= 'document.getElementById("papercut-warn").style.display = "none";';
    $text .= '}';
    $text .= 'else if (pcUserDetails.balance >= 2) {';
    $text .= 'var pcBalancePercent = 100 / 3 * pcUserDetails.balance;';
    $text .= 'document.getElementsByClassName("block_papercut")[0].style.background = "#4d72a7";';
    $text .= 'document.getElementsByClassName("block_papercut")[0].childNodes[0].childNodes[0].childNodes[1].style.color = "#FFFFFF";';
    $text .= 'document.getElementById("papercut-ok").style.display = "none";';
    $text .= 'document.getElementById("papercut-progress-warning").style.display = "none";';
    $text .= 'document.getElementById("papercut-progress-danger").style.display = "none";';
    $text .= 'document.getElementById("papercut-message1").innerHTML = "' . get_string('papercut-ok-high', 'block_papercut') . '";';
    $text .= 'document.getElementById("papercut-message2").style.display = "none";';
    $text .= 'document.getElementById("papercut-progress-success").style.width = pcBalancePercent + "%";';
    $text .= '}';
    $text .= 'else if (pcUserDetails.balance >= 1) {';
    $text .= 'var pcBalancePercent = 100 / 3 * pcUserDetails.balance;';
    $text .= 'document.getElementsByClassName("block_papercut")[0].style.background = "#4d72a7";';
    $text .= 'document.getElementsByClassName("block_papercut")[0].childNodes[0].childNodes[0].childNodes[1].style.color = "#FFFFFF";';
    $text .= 'document.getElementById("papercut-ok").style.display = "none";';
    $text .= 'document.getElementById("papercut-progress-success").style.display = "none";';
    $text .= 'document.getElementById("papercut-progress-danger").style.display = "none";';
    $text .= 'document.getElementById("papercut-message1").innerHTML = "' . get_string('message1low', 'block_papercut') . '";';
    $text .= 'document.getElementById("papercut-message2").innerHTML = "' . get_string('message2low', 'block_papercut') . '";';
    $text .= 'document.getElementById("papercut-progress-warning").style.width = pcBalancePercent + "%";';
    $text .= '}';
    $text .= 'else if (pcUserDetails.balance > 0) {';
    $text .= 'var pcBalancePercent = 100 / 3 * pcUserDetails.balance;';
    $text .= 'document.getElementsByClassName("block_papercut")[0].style.background = "#4d72a7";';
    $text .= 'document.getElementsByClassName("block_papercut")[0].childNodes[0].childNodes[0].childNodes[1].style.color = "#FFFFFF";';
    $text .= 'document.getElementById("papercut-ok").style.display = "none";';
    $text .= 'document.getElementById("papercut-progress-success").style.display = "none";';
    $text .= 'document.getElementById("papercut-progress-warning").style.display = "none";';
    $text .= 'document.getElementById("papercut-message1").innerHTML = "' . get_string('message1verylow', 'block_papercut') . '";';
    $text .= 'document.getElementById("papercut-message2").innerHTML = "' . get_string('message2verylow', 'block_papercut') . '";';
    $text .= 'document.getElementById("papercut-progress-danger").style.width = pcBalancePercent + "%";';
    $text .= '}';
    $text .= 'else {';
    //$text .= 'document.getElementsByClassName("block_papercut")[0].style.background = "#4d72a7";';
    //$text .= 'document.getElementsByClassName("block_papercut")[0].childNodes[0].childNodes[0].childNodes[1].style.color = "#FFFFFF";';
    $text .= 'document.getElementById("papercut-ok").style.display = "none";';
    $text .= 'document.getElementById("papercut-progress").style.display = "none";';
    if (is_numeric(substr($USER->username, 0 , 2))) {
        $text .= 'document.getElementById("papercut-message1").innerHTML = "' . get_string('message1none', 'block_papercut') . '";';
        $text .= 'document.getElementById("papercut-message2").innerHTML = "' . get_string('message2none', 'block_papercut') . '";';
        $text .= 'document.getElementById("papercut-message1").style.color = "#ff0033";';
        $text .= 'document.getElementById("papercut-message1").style.fontWeight = "bold";';
        $text .= 'document.getElementById("papercut-message2").style.color = "#333";';
    } else {
        $text .= 'document.getElementById("papercut-message1").innerHTML = "";';
        $text .= 'document.getElementById("papercut-message2").innerHTML = "' . get_string('message2staff', 'block_papercut') . '";';
        $text .= 'document.getElementById("papercut-message1").style.color = "#333";';
        $text .= 'document.getElementById("papercut-message2").style.color = "#333";';
    }
    $text .= '}';
    $text .= '</script>'; //close script tag

    $this->content         =  new stdClass;
    $this->content->text   = $text;
    //$this->content->footer = 'Footer here...';
 
    return $this->content;
  }
}   // Here's the closing bracket for the class definition
