<?php

defined('MOODLE_INTERNAL') || die;


$settings->add(new admin_setting_configtext(
            'papercut/ServerName',
            get_string('labelservername', 'block_papercut'),
            get_string('descdbservername', 'block_papercut'),
            ''
        ));