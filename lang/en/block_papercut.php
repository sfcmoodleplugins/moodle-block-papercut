<?php
$string['pluginname'] = 'Papercut Widget block';
$string['papercut'] = 'Print Credit';
$string['papercut:addinstance'] = 'Add a new Papercut Widget block';
$string['papercut:myaddinstance'] = 'Add a new Papercut Widget block to the My Moodle page';

$string['labelservername'] = 'Server Name:';
$string['descdbservername'] = 'Papercut server FQDN';

$string['papercut-ok-default'] = 'You have plenty of Print Credit remaining';
$string['papercut-ok-high'] = 'Your print credit is OK';
$string['message1low'] = 'Your print credit is running low';
$string['message2low'] = 'You may need to contact your Senior Tutor to request additional print credit';
$string['message1verylow'] = 'Your print credit is very low';
$string['message2verylow'] = 'Please contact your Senior Tutor to request additional print credit';
$string['message1none'] = 'No Print Credit remaining';
$string['message2none'] = 'Please contact your Senior Tutor to request additional print credit';
$string['message2staff'] = 'Staff do not use Print Credit';
